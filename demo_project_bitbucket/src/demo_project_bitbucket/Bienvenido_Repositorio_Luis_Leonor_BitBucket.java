
package demo_project_bitbucket;

import java.util.Scanner;

/**
 *
 * @author  Luis E. Leonor
 */
public class Bienvenido_Repositorio_Luis_Leonor_BitBucket {
    public static void main(String[] args) {
         
        //Nos aparece un cuadro de dialogo
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduzca su nombre");
        String nombre=sc.nextLine();
  
        System.out.println("Bienvenido "+nombre);
    }
     
}
    